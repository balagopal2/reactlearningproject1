import React from 'react';
import './App.css';
import Main from './pages/index.jsx';

export const App = ()=> {
  return (
      <div className="App">
        <Main />
      </div>
  );
}
