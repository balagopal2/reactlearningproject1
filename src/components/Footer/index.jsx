import {Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
    return (
        <div className='bg-dark py-4'>
            <Container>
                <Row>
                    <Col sm={5} className='text-start'>
                        <label className='text-white form-label'>Address</label>
                        <ul>
                            <li className='text-white'>Test address</li>
                        </ul>
                    </Col>
                    <Col sm={7} className='text-start'>
                        <label className='text-white form-label'>News Letter</label>
                        <div className='form-group'>
                            <form>
                                <div className="mb-3">
                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                                </div>
                                <button type="submit" className="btn btn-primary">Subscribe</button>
                            </form>
                        </div>
                    </Col>
                    <Col sm={12} className='mt-3'>Social media link</Col>
                </Row>
            </Container>
        </div>
    );
}
export default Footer;