import { Button, Container, Form, Nav, Navbar } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';

const Header = () => {
    const navigate = useNavigate();

    const [loginCheck, setLoginCheck] = useState("");

    useEffect(() => {
        setLoginCheck(localStorage.getItem('username'));
        const userCheck = localStorage.getItem("username");
        if (userCheck) {
            navigate('/dashboard');
        } else {
            navigate('/sign-in');
        }
    }, [])

    const logoutHandler = () => {
        localStorage.removeItem('username');
        navigate('/sign-in');
    };

    return (
        <Navbar expand="md" className="bg-body-tertiary">
            <Container>
                <Link as={Link} to="/"><img src={process.env.PUBLIC_URL + '/images/logo.png'} style={{ maxHeight: '70px' }} /> </Link>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/about-us">About us</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                        <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                        />
                        {loginCheck !== "" || loginCheck !== null ? <Button onClick={logoutHandler} variant="outline-danger" className='text-nowrap'>Logout</Button> : <Button onClick={() => navigate("/sign-in")} variant="outline-primary" className='text-nowrap'>Sign In</Button>}
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;