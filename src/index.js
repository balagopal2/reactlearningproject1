import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  createBrowserRouter,
  RouterProvider,
  Outlet
} from "react-router-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import { App } from './App.jsx';
import SignIn from './pages/Authentication/index.jsx';
import Dashboard from './pages/Admin/index.jsx';
import AboutUs from './pages/AboutUs/index.jsx';
import NewsDetails from './pages/News/NewsDetails/index.jsx';

const router = createBrowserRouter([
  { 
    path:"/",
    element: (
      <div>
        <Header />
        <Outlet />
        <Footer />
      </div>
    ),
    children: [
      {
        path: "",
        element: <App />
      },
      {
        path: "news-details",
        element: <NewsDetails />
      },
      {
        path: "about-us",
        element: <AboutUs />
      }, 
      {
        path: "dashboard",
        element: <Dashboard />
      },
    ]
  },
  { 
    path: "sign-in",
    element: <SignIn />,
  }
  
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
