import { Container } from "react-bootstrap";

const AboutUs = () => {
    return (
        <Container>
            <div className="about-banner py-5">
                <h1>About Us</h1>
            </div>
        </Container>
    )
}
export default AboutUs;