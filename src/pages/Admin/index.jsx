import { Container } from "react-bootstrap";

const Dashboard = () => {
    return(
        <Container className="vh-50">
            <div className="text-center p-5">
                <h1>Dashboard</h1>
            </div>
        </Container>
    )
}
export default Dashboard;