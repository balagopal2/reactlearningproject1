import { useEffect, useState } from "react";
import { Button, Card, Col, Form, Row } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

const SignIn = () => {
    const navigate = useNavigate();
    const [data, setData] = useState({
        username: "",
        password: ""
    })

    const [existUser, setExistUser] = useState([{
        existEmail: 'balu@gmail.com',
        existPass: '12345'
    }, {
        existEmail: 'nandu@gmail.com',
        existPass: '123456'
    }, {
        existEmail: 'arun@gmail.com',
        existPass: '1234567'
    }])

    const [error, setError] = useState('');

    const { username, password } = data;

    const handleChange = e => {
        setData({ ...data, [e.target.name]: e.target.value });
    }

    const handleSubmit = e => {
        e.preventDefault();
        const isValidUser = existUser.some(user => user.existEmail === username && user.existPass === password);

        if (isValidUser) {
            localStorage.setItem('username', username);
            navigate('/dashboard');
        } else {
            setError('Incorrect username or password');
        }
    }

    useEffect(() => {
        const currentUser = localStorage.getItem("username");
        if (currentUser) {
            navigate("/dashboard");
        } else {
            navigate("/sign-in")
        }
    }, [])

    return (
        <div className="d-grid align-center p-5 mt-5">
            <Row className="justify-content-center">
                <Col md={5}>
                    <Card className="signin-card p-4">
                        <Card.Body>
                            <Card.Title className="mb-4">Sign In</Card.Title>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group className="mb-3" controlId="formGroupEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control type="email" placeholder="Enter email" name="username" value={username} onChange={handleChange} />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formGroupPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" name="password" value={password} onChange={handleChange} />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                                <br />
                                <Link as={Link} to={"/"} className="mt-2 d-inline-block">Back to Home</Link>
                                {error && <div className="text-danger">{error}</div>}
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}
export default SignIn;