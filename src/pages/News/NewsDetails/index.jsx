import { Container } from "react-bootstrap";
import { Link, useLocation } from 'react-router-dom';

const NewsDetails = () => {
    const { state } = useLocation();

    if (!state) {
        return <div>Loading...</div>;
    }
    
    return (
        <Container>
            <div className="news-wrapper py-5">
                <Link to={"/"} className="text-dark mb-2 d-inline-block text-decoration-none">Back</Link>
                <div className="news-cover border">
                    <img className="w-100 h-100" src={state.urlToImage} alt="" />
                </div>
                <div className="news-content mt-4">
                    <h2>{state.title}</h2>
                    <span className="text-primary">{state.author}</span>
                    <p>{state.description}</p>
                </div>

            </div>
        </Container>
    )
}
export default NewsDetails;