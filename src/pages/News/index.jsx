import { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const NewsListing = () => {

    const [news, setNews] = useState([]);

    const fetchData = async () => {
        const data = await fetch('https://newsapi.org/v2/everything?domains=wsj.com&apiKey=68364cb182b34052b00dca34c111c51a&pageSize=9');
        const jsonData = await data.json();
        setNews(jsonData.articles)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return <Row>
        {
            news.map((item, index) => (
                <Col sm={6} md={4} className='mb-4' key={index}>
                    <Card className='text-start h-100'>
                        <Card.Img variant="top" src={item.urlToImage} className='news-img' />
                        <Card.Body>
                            <Card.Title>{item.title}</Card.Title>
                            <Card.Text>{item.description}</Card.Text>
                            <Link to={"/news-details"} state={item} className='btn btn-primary'>Show more</Link>
                        </Card.Body>
                    </Card>
                </Col>
            ))
        }
    </Row>
}
export default NewsListing;