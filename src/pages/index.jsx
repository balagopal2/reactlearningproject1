import { useState } from 'react';
import { Container, Row, Col, Card, Form } from 'react-bootstrap';
import NewsListing from './News';

const Main = () => {

    const [number, setNumber] = useState(50);
    const [button, setButton] = useState(true);
    const [checkbox, setCheckbox] = useState(true);

    function buttonClick() {
        setButton(!button);
    }

    function handleChange() {
        setCheckbox(!checkbox);
    }

    function addNumber() {
        setNumber(number => number + 1);
    }

    return <div className="main py-5">
        <Container>
            <button onClick={buttonClick}>
                {button ? 'true' : 'false'}
            </button>
            <Form className='my-5 text-start'>
                <Form.Check
                    type="switch"
                    id="custom-switch"
                    onChange={handleChange}
                />
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    {checkbox ? <Form.Control type="email" placeholder="name@example.com" /> : ""}
                </Form.Group>
            </Form>
            <Row className='mb-4'>
                <Col sm={6} md={4}>
                    <Card className='text-start'>
                        <Card.Body>
                            <Card.Title>Contact Form</Card.Title>

                            <input type="text" value={number} onChange={(e) => {
                                setNumber(e.target.value);
                            }} />
                            <div className='d-flex gap-2 mt-2'>
                                <button className="btn btn-primary" onClick={() => {
                                    addNumber();
                                    addNumber();
                                    addNumber();
                                }}>Add 3</button>

                                <button className="btn btn-primary" onClick={addNumber}>Add 1</button>
                            </div>

                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <NewsListing />
        </Container>
    </div>
}
export default Main;